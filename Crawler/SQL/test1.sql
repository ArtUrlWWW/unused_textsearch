-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 30, 2012 at 01:47 PM
-- Server version: 5.6.7-rc
-- PHP Version: 5.4.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `test1`
--

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE IF NOT EXISTS `links` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(777) NOT NULL,
  `linkName` varchar(777) NOT NULL DEFAULT '',
  `type` varchar(777) NOT NULL DEFAULT 'page',
  `LastModified` varchar(777) NOT NULL DEFAULT '',
  `ContentLength` varchar(777) NOT NULL DEFAULT '',
  `processedAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `processed` int(11) NOT NULL DEFAULT '0',
  `processedBy` varchar(777) NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
