package name.khartn.ftspc.indexer.net.utils;

import java.net.URLConnection;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class FileTypesUtil {

    public static Boolean isHTMLorXML(URLConnection connection) {
        String contentType = connection.getContentType();

        if (contentType.contains("text/html")
                || contentType.contains("application/xml")
                || contentType.contains("application/xhtml+xml")) {
            return true;
        } else {
            return false;
        }

    }
}
