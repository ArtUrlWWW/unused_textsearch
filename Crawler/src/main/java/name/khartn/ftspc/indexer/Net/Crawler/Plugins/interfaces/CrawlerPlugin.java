/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package name.khartn.ftspc.indexer.Net.Crawler.Plugins.interfaces;

import org.jsoup.nodes.Document;

/**
 *
 * @author wwwdev
 */
public interface CrawlerPlugin {
     public void processPage(Document doc);
     public Boolean excludePage(String url);
}
