package name.khartn.ftspc.indexer.net.crawler;

import name.khartn.ftspc.indexer.Net.Crawler.Plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.crawler.plugins.DefaultPlugin;
import org.apache.commons.lang3.text.WordUtils;

/**
 * Crawler's main class
 *
 * @author Arthur Khusnutdinov
 */
public class Crawler {

    public static String mainURL;
    public static CrawlerPlugin crawlerPlugin = null;

    public Crawler(String mainURL) {
        this.mainURL = mainURL;
    }

    public void Main() {
        processSite(mainURL);
    }

    public void processSite(String siteURL) {
        try {
            mainURL = siteURL;

            Vars.logger.info("Started process of the site " + siteURL + " indexing.");
            String module = siteURL.replace("http://www.", "");
            module = module.replace("http://", "");
            module = module.substring(0, module.indexOf("/"));
            module = WordUtils.capitalize(module);
            try {
                crawlerPlugin = (CrawlerPlugin) Class.forName("name.khartn.ftspc.indexer.Net.Crawler.Plugins." + module.replaceAll("\\.", "_")).newInstance();
            } catch (ClassNotFoundException ex) {
                Vars.logger.info(ex);
            }

            if (Crawler.crawlerPlugin == null) {
                crawlerPlugin = new DefaultPlugin();
            }

            CrawlersProcessPageThread cppt = new CrawlersProcessPageThread(siteURL, crawlerPlugin, "");
            cppt.start();
//            cppt.processPage(siteURL);

        } catch (InstantiationException | IllegalAccessException ex) {
            Vars.logger.fatal(ex);
        }

    }
}
