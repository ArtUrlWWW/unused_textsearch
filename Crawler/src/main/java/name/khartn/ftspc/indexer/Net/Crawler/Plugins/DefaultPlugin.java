package name.khartn.ftspc.indexer.net.crawler.plugins;

import name.khartn.ftspc.indexer.Net.Crawler.Plugins.interfaces.CrawlerPlugin;
import org.jsoup.nodes.Document;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class DefaultPlugin implements CrawlerPlugin {

    @Override
    public void processPage(Document doc) {
    }

    @Override
    public Boolean excludePage(String url) {
        return false;
    }
    
}
