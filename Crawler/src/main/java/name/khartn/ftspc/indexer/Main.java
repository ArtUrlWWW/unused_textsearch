package name.khartn.ftspc.indexer;

import java.sql.SQLException;
import name.khartn.ftspc.indexer.ini_and_vars.Lector_Ini;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.crawler.Crawler;
import name.khartn.ftspc.indexer.net.utils.ThreadsUtil;
import name.khartn.ftspc.indexer.schedulled.TasksStarter;
import name.khartn.ftspc.indexer.utils.files.DirRemover;

/**
 * Main Class
 *
 * @author Arthur Khusnutdinov
 */
public class Main {
//-Xmx1700m -Xms1500m
//-Xmx300m -Xms500m

    /**
     * Main function
     *
     * @param args Not used.
     */
    public static void main(String[] args) {
        try {

            DirRemover dirRemover = new DirRemover();
            dirRemover.remove("logs");
            dirRemover.remove("demobase");

            Lector_Ini.class.newInstance().configure();
            Crawler crawler = new Crawler("http://ihtik.lib.ru/");
//            Crawler crawler = new Crawler("http://bta-kazan.ru/");
            crawler.Main();

            TasksStarter tasksStarter = new TasksStarter();
            tasksStarter.start();

            ThreadsUtil threadsUtil = new ThreadsUtil();
            threadsUtil.waitUntilQuartzWorkerIsFinished();
            threadsUtil.waitForThreadsAtEnd();

            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        System.out.println("all count is " + Vars.TMPALLCOUNTER);
                        System.out.println("exiting now...");
                        Vars.hSQLDBHandler.executeUpdate("SHUTDOWN");
                        Vars.hSQLDBHandler.getConnection().close();
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
            });

        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
