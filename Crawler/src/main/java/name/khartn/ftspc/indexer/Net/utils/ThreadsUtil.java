package name.khartn.ftspc.indexer.net.utils;

import java.util.Calendar;
import java.util.Date;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;

/**
 *
 * @author wwwdev
 */
public class ThreadsUtil {

    public void waitForThreads() {
        Date startTime = new Date();

        Calendar c = Calendar.getInstance();
        c.setTime(startTime);
        c.add(Calendar.SECOND, 25);
        startTime = c.getTime();

        while (Vars.threadsCount > Vars.max_threads) {

            synchronized (this) {
                try {
                    wait(500);
                } catch (InterruptedException ex) {
                    Vars.logger.fatal("Error: ", ex);
                }
            }

            Date secondTime = new Date();
            if (secondTime.getTime() > startTime.getTime()) {
                break;
            }
        }
    }

    public void waitForThreadsAtEnd() {
        synchronized (this) {
            try {
                wait(15000);
            } catch (InterruptedException ex) {
                Vars.logger.fatal("Error: ", ex);
            }
        }
        while (Vars.threadsCount > 0) {

            synchronized (this) {
                try {
                    wait(15000);
                } catch (InterruptedException ex) {
                    Vars.logger.fatal("Error: ", ex);
                }
            }

        }
        synchronized (this) {
            try {
                wait(15000);
            } catch (InterruptedException ex) {
                Vars.logger.fatal("Error: ", ex);
            }
        }
        while (Vars.threadsCount > 0) {

            synchronized (this) {
                try {
                    wait(15000);
                } catch (InterruptedException ex) {
                    Vars.logger.fatal("Error: ", ex);
                }
            }

        }
    }

    public void waitUntilQuartzWorkerIsFinished() {

        synchronized (this) {
            try {
                wait(80000);
            } catch (InterruptedException ex) {
                Vars.logger.fatal("Error: ", ex);
            }
        }

        while (Vars.quartzSchedulledLinksProcessorIsAlreadyStarted) {
            synchronized (this) {
                try {
                    wait(5000);
                } catch (InterruptedException ex) {
                    Vars.logger.fatal("Error: ", ex);
                }
            }
        }
        System.out.println("No quartz workers works now... Exiting...");
    }
}
