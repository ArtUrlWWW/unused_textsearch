package name.khartn.ftspc.indexer.ini_and_vars;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Properties;
import java.util.Scanner;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * Class for loading settings from the ini file to the public static variables.
 *
 * @author Arthur Khusnutdinov
 *
 */
public class Lector_Ini {

    /**
     * Method for loading settings from the ini file to the public static
     * variables.
     */
    private void prepareIniFile() {
        StringBuilder text = new StringBuilder();
        String NL = System.getProperty("line.separator");
        Scanner scanner = null;
        Writer out = null;
        try {
            scanner = new Scanner(new FileInputStream("main_config.ini"), "UTF-8");
            while (scanner.hasNextLine()) {
                text.append(scanner.nextLine()).append(NL);
            }
            out = new OutputStreamWriter(
                    new FileOutputStream("main_config.ini"), "UTF-8");
            out.write(text.toString().replace("\\", "/"));
        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        } finally {
            try {
                scanner.close();
                out.close();
            } catch (Exception ex) {
            }
        }
    }

    /**
     * The method of settings initialization.
     */
    public void configure() {
        try {
            prepareIniFile();

            Properties keeper = new Properties();
            keeper.load(new FileInputStream("main_config.ini"));

            Vars.mysqlHost = keeper.getProperty("mysql_host");
            Vars.mysqlPort = keeper.getProperty("mysql_port");
            Vars.mysqlDb = keeper.getProperty("mysql_db");
            Vars.mysqlUser = keeper.getProperty("mysql_user");
            Vars.mysqlPassword = keeper.getProperty("mysql_passwd");
//            Vars.PathToTmpDir = keeper.getProperty("PathToTmpDir");
            Vars.ConnectionPool.MaxPoolSize = Integer.parseInt(keeper.getProperty("MaxPoolSize"));
            Vars.ConnectionPool.MinPoolSize = Integer.parseInt(keeper.getProperty("MinPoolSize"));
            Vars.ConnectionPool.AcquireIncrement = Integer.parseInt(keeper.getProperty("AcquireIncrement"));
            Vars.max_threads = Integer.parseInt(keeper.getProperty("max_threads"));
            Vars.useCookies = Integer.parseInt(keeper.getProperty("useCookies"));

            PropertyConfigurator.configure(props());
            Vars.logger = Logger.getRootLogger();

//            if (Vars.PathToTmpDir == null) {
//                File tmpDir = new File("");
//                tmpDir = new File(tmpDir.getAbsolutePath() + "/temp");
//                if (tmpDir.mkdirs()) {
//                    Vars.logger.info("Temporary directory successfully created");
//                } else {
//                    Vars.logger.warn("Temporary directory was not created!");
//                }
//                Vars.PathToTmpDir = tmpDir.getAbsolutePath().replace("\\", "/");
//                tmpDir = null;
//            }


        } catch (Exception ex) {
            ex.printStackTrace();
            Vars.logger.fatal("Error: ", ex);
        }
    }

    private static Properties props() {
        Properties props = new Properties();
        props.put("log4j.rootLogger", "INFO, R");
        props.put("log4j.appender.R",
                "org.apache.log4j.DailyRollingFileAppender");
        props.put("log4j.appender.R.File", "logs/Main.log");
        props.put("log4j.appender.R.Append", "true");
        props.put("log4j.appender.R.Threshold", "INFO");
        props.put("log4j.appender.R.DatePattern", "'.'yyyy-MM-dd");
        props.put("log4j.appender.R.layout", "org.apache.log4j.PatternLayout");
        props.put("log4j.appender.R.layout.ConversionPattern",
                "[%5p] %d{yyyy-MM-dd mm:ss} %c (%F:%M:%L)%n%m%n");
        return props;
    }
}
