package name.khartn.ftspc.indexer.ini_and_vars;

import java.util.Map;
import name.khartn.ftspc.indexer.DataBases.HSQLDBHandler;
import org.apache.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;

/**
 * Class with the static (global) variables
 *
 * @author Arthur Khusnutdinov
 */
public class Vars {

    public static class DataBase {
    }

    public static class ConnectionPool {

        public static Integer MaxPoolSize = 0;
        public static Integer MinPoolSize = 0;
        public static Integer AcquireIncrement = 0;
    }
/////////////////////////////////////////////////////////////////
//    MySQL Section
/////////////////////////////////////////////////////////////////
    /**
     * MySQL host
     */
    public static String mysqlHost = "";
    /**
     * MySQL port
     */
    public static String mysqlPort = "";
    /**
     * MySQL database for use
     */
    public static String mysqlDb = "";
    /**
     * MySQL user name
     */
    public static String mysqlUser = "";
    /**
     * MySQL user's password
     */
    public static String mysqlPassword = "";
    /**
     * Logger object.
     */
    public static Logger logger = null;
    /**
     * An object of the scheduler.
     */
//    public static Scheduler sched = null;
    /**
     * The path to the directory for temporary files.
     */
//    public static String PathToTmpDir;
    public static HSQLDBHandler hSQLDBHandler = new HSQLDBHandler();
    public static Integer threadsCount = 0;
    public static Integer max_threads = 0;
    public static Map<String, String> cookies = null;
    public static Integer useCookies = 0;

    // NEW!!!
    public static SchedulerFactory sf = new StdSchedulerFactory();
    public static Scheduler sched;
    public static Boolean quartzSchedulledLinksProcessorIsAlreadyStarted=false;
    public static Integer doneIndexingCounter=0;
    public static Integer TMPALLCOUNTER=0;
}
