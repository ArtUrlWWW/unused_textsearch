package name.khartn.ftspc.indexer.schedulled.tasks;

import java.sql.ResultSet;
import java.sql.Statement;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import static name.khartn.ftspc.indexer.net.crawler.Crawler.crawlerPlugin;
import name.khartn.ftspc.indexer.net.crawler.CrawlersProcessPageThread;
import name.khartn.ftspc.indexer.net.utils.ThreadsUtil;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class SchedulledLinksProcessor implements Job {

    /**
     * <p>
     * Empty constructor for job initilization
     * </p>
     * <p>
     * Quartz requires a public empty constructor so that the scheduler can
     * instantiate the class whenever it needs.
     * </p>
     */
    public SchedulledLinksProcessor() {
    }

    /**
     * <p>
     * Called by the <code>{@link org.quartz.Scheduler}</code> when a
     * <code>{@link org.quartz.Trigger}</code> fires that is associated with the
     * <code>Job</code>.
     * </p>
     *
     * @throws JobExecutionException if there is an exception while executing
     * the job.
     */
    public void execute(JobExecutionContext context) throws JobExecutionException {
        if (!Vars.quartzSchedulledLinksProcessorIsAlreadyStarted) {
            Vars.quartzSchedulledLinksProcessorIsAlreadyStarted = true;

            try {
                ThreadsUtil threadsUtil = new ThreadsUtil();

                Vars.sched.pauseAll();

                System.out.println("Starting processor of the quartz");

// TODO Переделать логику работы с SchedulledLinks - создать отдельный воркер, например, на quartz
                java.sql.Connection dbConn;
                dbConn = Vars.hSQLDBHandler.getConnection();
                String sql = "SELECT * FROM SchedulledLinks where processedAt='1900-01-01 00:00:00'";
                Statement statement = dbConn.createStatement();
                ResultSet resultSetLocal = statement.executeQuery(sql);

                Integer rowsCounter = 0;
                while (resultSetLocal.next()) {
                    rowsCounter++;
                    String foundedUrl = resultSetLocal.getString("link");
                    String foundedUrlLinkName = resultSetLocal.getString("linkName");
                    threadsUtil.waitForThreads();

                    CrawlersProcessPageThread cppt = new CrawlersProcessPageThread(foundedUrl, crawlerPlugin, foundedUrlLinkName);
                    cppt.start();
                }

                synchronized (this) {
                    try {
                        wait(5000);
                    } catch (InterruptedException ex) {
                        Vars.logger.fatal("Error: ", ex);
                    }
                }
                while (Vars.threadsCount > 0) {

                    synchronized (this) {
                        try {
                            wait(5000);
                        } catch (InterruptedException ex) {
                            Vars.logger.fatal("Error: ", ex);
                        }
                    }

                }

                Vars.sched.resumeAll();

                if (rowsCounter < 1) {
                    Vars.doneIndexingCounter++;
                    if (Vars.doneIndexingCounter > 10) {
                        Vars.sched.shutdown();
                        System.out.println("Site indexing is done.");
                    }
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            Vars.quartzSchedulledLinksProcessorIsAlreadyStarted = false;

        }
    }

}
