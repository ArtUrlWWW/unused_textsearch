/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package name.khartn.ftspc.indexer.schedulled;

import java.util.Date;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.schedulled.tasks.SchedulledLinksProcessor;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;

/**
 *
 * @author wwwdev
 */
public class TasksStarter {

    public void start() {
        try {
            Vars.sched = Vars.sf.getScheduler();
            
            JobDetail job = new JobDetail("job1", "group1", SchedulledLinksProcessor.class);
            CronTrigger trigger = new CronTrigger("trigger1", "group1", "job1", "group1", "0/10 * * * * ?");
            Vars.sched.addJob(job, true);
            Date ft = Vars.sched.scheduleJob(trigger);
            Vars.logger.info(job.getFullName() + " has been scheduled to run at: " + ft + " and repeat based on expression: " + trigger.getCronExpression());
            
            Vars.sched.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
