package name.khartn.ftspc.indexer.Net.Crawler.Plugins;

import name.khartn.ftspc.indexer.Net.Crawler.Plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class Ihtik_lib_ru implements CrawlerPlugin {

    @Override
    public void processPage(Document doc) {
        try {
            String pageUrl = doc.baseUri();
            Integer counter = 0;

            Elements tables = doc.getElementsByTag("table");
            if (tables.size() > 0) {
                String[] tmpStrArr;
                tmpStrArr = pageUrl.replace("http://", "").split("/");
                pageUrl += "/" + tmpStrArr[1] + "_";
                String link;

                for (Element table : tables) {
                    Elements trs = table.getElementsByTag("tr");
                    for (Element tr : trs) {
                        Elements tds = tr.select("td:eq(0)");
                        if (tds.size() > 0) {
                            Element td = tds.get(0);

                            String tdText = td.text();
                            link = pageUrl + tdText.substring(0, tdText.indexOf(".")) + ".rar";
                            if (Vars.hSQLDBHandler.indexOfLink(link) == null) {
                                String linkName = tdText.substring(tdText.indexOf(".")+1);
                                linkName = linkName.replaceAll("'", "''");
                                counter++;
                                String sqlQuery = "INSERT INTO links(link, type, linkName) VALUES ('" + link + "', 'file', '" + linkName + "');";
                                Vars.hSQLDBHandler.executeUpdate(sqlQuery);
                            }

                        }
                    }
                }
            }
            
            Vars.hSQLDBHandler.commit();

            System.out.println("ULR " + pageUrl + " count " + counter);            
            Vars.TMPALLCOUNTER+=counter;
            System.out.println("all count is " + Vars.TMPALLCOUNTER);
        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
            ex.printStackTrace();
        }
    }

    @Override
    public Boolean excludePage(String url) {
        if (url.contains("_catalog_ihtik.lib.ru_")) {
            return true;
        } else {
            return false;
        }
    }
}
