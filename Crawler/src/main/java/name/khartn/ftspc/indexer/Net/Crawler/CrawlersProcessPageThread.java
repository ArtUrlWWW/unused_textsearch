package name.khartn.ftspc.indexer.net.crawler;

import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import name.khartn.ftspc.indexer.Net.Crawler.Plugins.interfaces.CrawlerPlugin;
import name.khartn.ftspc.indexer.ini_and_vars.Vars;
import name.khartn.ftspc.indexer.net.utils.FileTypesUtil;
import name.khartn.ftspc.indexer.net.utils.ThreadsUtil;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author Arthur Khusnutdinov
 */
public class CrawlersProcessPageThread extends Thread {

    String secondUrl;
    CrawlerPlugin crawlerPlugin;
    String linkName;

    public CrawlersProcessPageThread(String secondUrl, CrawlerPlugin crawlerPlugin, String linkName) {
        this.secondUrl = secondUrl;
        this.crawlerPlugin = crawlerPlugin;
        this.linkName = linkName;
    }

    @Override
    public void run() {

        ThreadsUtil threadsUtil = new ThreadsUtil();
        threadsUtil.waitForThreads();

        Vars.threadsCount++;
        this.processPage(secondUrl, linkName);
        Vars.threadsCount--;
    }

    public void processPage(String pageUrl, String linkName) {
        try {

            if (Vars.hSQLDBHandler.indexOfSchedulledLink(pageUrl) == null) {
                String sqlQuery = "INSERT INTO SchedulledLinks(link, ParentLink, linkName) VALUES ('"
                        + pageUrl.replaceAll("'", "\\\\'") + "', '" + linkName.replaceAll("'", "\\\\'") + "', 'NON!');";
                Vars.hSQLDBHandler.executeUpdate(sqlQuery);
                Vars.hSQLDBHandler.getConnection().commit();
            }

            URL url = new URL(pageUrl);
            URLConnection connection = url.openConnection();
            String sqlQuery;

            if (FileTypesUtil.isHTMLorXML(connection)) {
                Document doc = null;
                String lastModified;
                String contentLength;
                Connection jsoupConnection = null;
                try {
                    jsoupConnection = Jsoup.connect(pageUrl).maxBodySize(20971520); //1024*1024*20
                    if (Vars.useCookies == 1 && Vars.cookies != null) {
                        doc = jsoupConnection.timeout(70000).cookies(Vars.cookies).get();
                    } else {
                        doc = jsoupConnection.timeout(70000).get();
                    }
                } catch (Exception ex) {
                    Vars.logger.warn("First Error " + secondUrl + " ", ex);
                    try {
                        jsoupConnection = Jsoup.connect(pageUrl);
                        if (Vars.useCookies == 1 && Vars.cookies != null) {
                            doc = jsoupConnection.timeout(70000).cookies(Vars.cookies).get();
                        } else {
                            doc = jsoupConnection.timeout(70000).get();
                        }
                    } catch (Exception ex1) {
                        Vars.logger.warn("Second Error " + pageUrl + " ", ex1);
                    }
                }

                if (doc != null) {
                    if (Vars.useCookies == 1 && Vars.cookies == null) {
                        Vars.cookies = jsoupConnection.response().cookies();
                    }

                    Map<String, String> headers = jsoupConnection.response().headers();
                    contentLength = headers.get("Content-Length");
                    lastModified = headers.get("Last-Modified");

                    if (contentLength == null) {
                        contentLength = "unknown";
                    }
                    if (lastModified == null) {
                        lastModified = "unknown";
                    }

                    Elements links = doc.getElementsByTag("a");
                    String foundedUrl;

                    Vars.logger.info("Page " + pageUrl + " " + this.getId());

                    for (Element link : links) {
                        foundedUrl = link.attr("abs:href");

                        if (!foundedUrl.equals(Crawler.mainURL)
                                && !foundedUrl.equals(Crawler.mainURL + "/")
                                && foundedUrl.contains(Crawler.mainURL)
                                && !crawlerPlugin.excludePage(foundedUrl)) {

                            if (Vars.hSQLDBHandler.indexOfSchedulledLink(foundedUrl) == null) {
                                Vars.logger.info("foundedUrl " + foundedUrl);
                                sqlQuery = "INSERT INTO SchedulledLinks(link, ParentLink, linkName) VALUES ('"
                                        + foundedUrl.replaceAll("'", "\\\\'") + "', "
                                        + "'" + pageUrl.replaceAll("'", "\\\\'") + "', "
                                        + "'" + link.text().replaceAll("'", "\\\\'") + "'"
                                        + ");";
                                Vars.logger.info("SQL " + sqlQuery);
                                Vars.hSQLDBHandler.executeUpdate(sqlQuery);
                                Vars.hSQLDBHandler.commit();
                            }
                        }
                    }

//                    System.out.println("Processing page " + pageUrl);
                    crawlerPlugin.processPage(doc);

                    doc = null;
                }

            }
//            else {
//                sqlQuery = "INSERT INTO links(link, type) VALUES ('" + pageUrl + "', 'unknown');";
//                Vars.hSQLDBHandler.insertSingle(sqlQuery);
//            }

            Vars.hSQLDBHandler.executeUpdate("update SchedulledLinks set processedAt=CURRENT_TIMESTAMP where link='" + secondUrl + "'");
            Vars.hSQLDBHandler.getConnection().commit();

        } catch (Exception ex) {
            Vars.logger.fatal("Error: ", ex);
        }
    }
}
